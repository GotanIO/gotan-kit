package io.gotan.kit.date;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * Fonction utilitaires pour la manipulation de dates.
 * 
 * @author Damien Cuvillier <damien@gotan.io> Built by [Gotan](http://gotan.io)
 *        @license Apache2
 * @since 4 janv. 2015
 */
public final class DateUtil {
	/** Constructeur privé. */
	private DateUtil() {

	}

	/** Nombre de secondes contenues en une heure. */
	public static final int SECONDS_PER_HOUR = 3600;
	/** Nombre de secondes contenues dans une minute. */
	public static final int SECONDS_PER_MINUTE = 60;
	/** Nombre de millisecondes dans une seconde. */
	public static final int MILLISECONDS_PER_SECOND = 1000;
	/** Nombre de millisecondes contenues en une heure. */
	public static final int MILLISECONDS_PER_HOUR = SECONDS_PER_HOUR
			* MILLISECONDS_PER_SECOND;
	/** Nombre de millisecondes contenues dans une minute. */
	public static final int MILLISECONDS_PER_MINUTE = SECONDS_PER_MINUTE
			* MILLISECONDS_PER_SECOND;
	/** Nombre d'heures dans une journée. */
	public static final int HOURS_PER_DAY = 24;

	/**
	 * Récupère la date courante.
	 * 
	 * @return la date courante
	 */
	public static Date now() {
		return new Date(new java.util.Date().getTime());
	}

	/**
	 * Récupère la date courante.
	 * 
	 * @return Timestamp
	 */
	public static Timestamp tnow() {
		return new Timestamp(now().getTime());
	}

	/**
	 * Récupère la date courante ± un delta.
	 * 
	 * @param deltaMs
	 *            Delta négatif ou positif exprimé en millisecondes.
	 * @return Date
	 */
	public static Date nowDelta(final long deltaMs) {
		return delta(now(), deltaMs);
	}

	/**
	 * Calcul un delta à partir d'une date d'origine.
	 * 
	 * @param dateToModify
	 *            Date d'origine du calcul.
	 * @param deltaMs
	 *            Nombre de millisecondes de différence. (peut-être négatif ou
	 *            positif)
	 * @return Date résultante du delta.
	 */
	public static Date delta(final Date dateToModify, final long deltaMs) {
		return new Date(dateToModify.getTime() + deltaMs);
	}

	/**
	 * Calcul un delta à partir d'une date d'origine.
	 * 
	 * @param tstp
	 *            Date d'origine du calcul.
	 * @param deltaMs
	 *            Nombre de millisecondes de différence. (peut-être négatif ou
	 *            positif)
	 * @return Date résultante du delta.
	 */
	public static Date delta(final Timestamp tstp, final int deltaMs) {
		return delta(new Date(tstp.getTime()), deltaMs);
	}

	/** Indique si la date fournie correspond à un jour travaillé. 
	 * 
	 * @param date Date à tester
	 * @param locale Pays concerné
	 * @return True / False
	 */
	public static boolean isWorkedDay(final java.util.Date date, final Locale locale) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		switch (cal.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.SUNDAY:
		case Calendar.SATURDAY:
			return false;
		default:
		}
		
		if (locale.equals(Locale.FRENCH) || locale.equals(Locale.FRANCE)) {
			for (int whiteDay:getFrenchHolidayBank(cal.get(Calendar.YEAR))) {
				if (cal.get(Calendar.DAY_OF_YEAR) == whiteDay) {
					return false;
				}
			}
		}
		return true;

	}
	
	public static final Set<Integer> getFrenchHolidayBank(int year){
		Set<Integer> dates = Sets.newHashSet();
		Calendar oneDate = new GregorianCalendar();
		oneDate.set(Calendar.YEAR, year);
		
		// Nouvel An
		dates.add(new GregorianCalendar(year,0,1).get(Calendar.DAY_OF_YEAR));
		
		// Fête du travail
		dates.add(new GregorianCalendar(year,4,1).get(Calendar.DAY_OF_YEAR));
		
		// Victoire 
		dates.add(new GregorianCalendar(year,4,8).get(Calendar.DAY_OF_YEAR));
		
		// Fête nationale 
		dates.add(new GregorianCalendar(year,6,14).get(Calendar.DAY_OF_YEAR));
		
		// Assomption 
		dates.add(new GregorianCalendar(year,7,15).get(Calendar.DAY_OF_YEAR));
		
		// Toussaint
		dates.add(new GregorianCalendar(year,10,1).get(Calendar.DAY_OF_YEAR));
		
		// Victoire
		dates.add(new GregorianCalendar(year,10,11).get(Calendar.DAY_OF_YEAR));
		
		// Noel 
		dates.add(new GregorianCalendar(year,11,25).get(Calendar.DAY_OF_YEAR));
	
		// Paques
		Calendar paques = calculLundiPaques(year);
		dates.add(paques.get(Calendar.DAY_OF_YEAR));
		
		// Ascencion
		paques.add(GregorianCalendar.DAY_OF_MONTH, 38);
		dates.add(paques.get(Calendar.DAY_OF_YEAR));
		
		// Pentecote
		paques.add(GregorianCalendar.DAY_OF_MONTH, 11);
		dates.add(paques.get(Calendar.DAY_OF_YEAR));
		
		
		return dates;
	}
	
	/** Récupère la date du lundi paques pour une année donnée.
	 *  
	 * @param annee Année concernée
	 * @return Date du lundi de paques
	 */
	public static GregorianCalendar calculLundiPaques(int annee) {
		int a = annee / 100;
		int b = annee % 100;
		int c = (3 * (a + 25)) / 4;
		int d = (3 * (a + 25)) % 4;
		int e = (8 * (a + 11)) / 25;
		int f = (5 * a + b) % 19;
		int g = (19 * f + c - e) % 30;
		int h = (f + 11 * g) / 319;
		int j = (60 * (5 - d) + b) / 4;
		int k = (60 * (5 - d) + b) % 4;
		int m = (2 * j - k - g + h) % 7;
		int n = (g - h + m + 114) / 31;
		int p = (g - h + m + 114) % 31;
		int jour = p + 1;
		int mois = n;

		GregorianCalendar date = new GregorianCalendar(annee, mois - 1, jour);
		date.add(GregorianCalendar.DAY_OF_MONTH, 1);
		return date;
	}
}
