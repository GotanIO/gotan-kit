/** Gestion de données temporelles. 
 * @author Damien Cuvillier <damien@gotan.io>  
 * Built by [Gotan](http://gotan.io)  
 *@license Apache2 
 * @since 4 janv. 2015
 */
package io.gotan.kit.date;