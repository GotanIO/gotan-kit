package io.gotan.kit.restlet;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/** Stockage de l'url liée à une entité.
 * .
 * <br />
 * @author Damien Cuvillier 
 * @since Novembre 2014
 */
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface UrlPath {
	/** Valeur de l'url. 
	 * Plusieurs valeurs peuvent être séparées par une virgule */
	String value();
}
