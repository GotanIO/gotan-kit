package io.gotan.kit.restlet;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

/** Representation Serialization d'une String representation Restlet.
 * Utile pour la mise en cache.
 * 
 * 
 * 
 * TODO Ajouter de nombreux paramètres sur les éléments sauvegardés (en cache) 
 * Tels que la langue, l'encodage, etc... 
 * 
 * 
 * @author Damien Cuvillier <damien@gotan.io>
 * @since 17 sept. 2013
 *
 *
 * 
 */
public class SerializedRepresentation implements Serializable {

	/** Auto Generated Serialization ID. */
	private static final long serialVersionUID = 3464625205633681274L;
	
	/** Date d'expiration. */
	private Date expirationDate;
	/** Type de media. */
	private String type;
	/** Contenu. */
	private String content;
	
	
	/** Constructeur. 
	 * @param sr Restlet String Representation.
	 * @throws IOException 
	 */
	public SerializedRepresentation(final Representation sr) throws IOException {
		this.content = sr.getText();
		this.type = sr.getMediaType().getName();
		sr.getExpirationDate();
	}
	
	/** Récupère la représentation restlet adéquate. 
	 * 
	 * @return Restlet String Representation
	 */
	public final StringRepresentation getRepresentation() {
		StringRepresentation sr = new StringRepresentation(this.content);
		sr.setMediaType(new MediaType(type));
		sr.setExpirationDate(this.expirationDate);
		return sr;
	}
}
