package io.gotan.kit.restlet;

import java.util.Set;

import org.restlet.data.Form;

/** Formulaire Restlet permettant de génériser le typage des valeurs postées.
 * .
 * <br />
 * Project DataStore <br /> <br />
 * All Rights Reserved - © Straton IT 
 * @author Damien Cuvillier <dcuvillier@straton-it.fr>
 * @since 5 juin 2013
 */
public final class FormUtils {
	
	/** Formulaire posté dans Reslet. 
	 */
	private Form form;
	
	/** Constructeur. 
	 * @param pForm Le formulaire Restlet. 
	 */
	public FormUtils(final Form pForm) {
		this.form = pForm;
	}
	
	
	/** Retourne la valeur pour un champ du formulaire qui doit être castée en entier. 
	 * Attention, si nombre flottant, renvoie null
	 * 
	 * @param formIdentifier Identifiant dans le formulaire.
	 * @return Entier
	 */
	public int integer(final String formIdentifier) {
		Integer i = getIntValue(form.getFirstValue(formIdentifier));
		return i == null ? 0 : i.intValue();
	}
	
	/** Retourne la valeur pour un champ du formulaire qui doit être castée en nombre flottant. 
	 * 
	 * @param formIdentifier Identifiant dans le formulaire.
	 * @return Flottant
	 */
	public float numeric(final String formIdentifier) {
		Float f = getNumericValue(form.getFirstValue(formIdentifier));
		return f == null ? 0.0F : f.floatValue();
	}
	
	
	
	/** Retourne la liste des valeurs pour un champ du formulaire qui doivent être castées en strings. 
	 *  Cas ou la valeur du champ est sous forme de tableau; <formIdentifier>: value1, value2, ..., valueN
	 * 
	 * @param formIdentifier Identifiant dans le formulaire.
	 * @return tableau de String
	 */
	
	public String[] strings(final String formIdentifier) {
		if(this.form.isEmpty()){
			throw new RuntimeException("it has no sense to look for values if the query is empty");
		}
		String strValues = form.getValues(formIdentifier);
		if(strValues == null){
			return null;
		}
		String [] strValuesArray = strValues.split(",");
		int length = strValuesArray.length;
		for(int i = 0; i < length; i++){
			strValuesArray[i] = strValuesArray[i].replaceAll("\\s+","");
		}
		return strValuesArray;			
	}
	
	/** Retourne la liste des valeurs pour un champ du formulaire qui doivent être castées en nombres entiers.
	 *  Cas ou la valeur du champ est sous forme de tableau; <formIdentifier>: int1, int2, ..., intN
	 * 
	 * @param formIdentifier Identifiant dans le formulaire.
	 * @return ArrayList<Integer>
	 * 
	 * @throw ClassCastException
	 */
	public int[] integers(final String formIdentifier){
		
		String[] strValuesArray = this.strings(formIdentifier);
		if(strValuesArray == null){
			return null;
		}
		int length = strValuesArray.length;
		int[] integers = new int[length];
		
		for(int i = 0; i < length; i++){
			
			Integer castedValue = getIntValue(strValuesArray[i]);
			if(castedValue == null){
				throw new ClassCastException("Unable to cast "+strValuesArray[i]+" to Integer");
			}
			integers[i] = castedValue;
		
		}
		return integers;
	}
	
	
	/** Retourne la valeur pour un champ du formulaire qui doit être castée en string. 
	 * 
	 * @param formIdentifier Identifiant dans le formulaire.
	 * @return La valeur 
	 * Null si vide
	 */
	public String string(final String formIdentifier) {
		return getStringValue(form.getFirstValue(formIdentifier));
	}
	

	/** Retourne la valeur pour un champ du formulaire qui doit être casté en boolean.
	 * 
	 * @param formIdentifier Identifiant dans le formulaire.
	 * Si le formulaire a été initialisé, alors il s'agit de la clé d'entrée dans le formulaire.
	 * @param tolerance : 
	 * si true, alors si la valeur est nulle, on force à false.
	 * @return La valeur castée
	 */
	public Boolean bool(final String formIdentifier, final boolean tolerance) {
		String value = form.getFirstValue(formIdentifier);
		if (value == null && tolerance) {
			return false;
		}
		return getBooleanValue(value);
	}
	/** Retourne la valeur pour un champ du formulaire qui doit être casté en boolean.
	 * 
	 * @param formIdentifier Identifiant dans le formulaire.
	 * Si le formulaire a été initialisé, alors il s'agit de la clé d'entrée dans le formulaire.
	 * @return La valeur castée
	 */
	public Boolean bool(final String formIdentifier) {
		return bool(formIdentifier, true);
	}
	
	/* ############ Methodes statiques de gestion des casts automatiques. ############# */
	
	/** Retourne la valeur pour un champ du formulaire qui doit être casté en entier.
	 * Si la valeur n'est pas à castée n'est pas un entier, la valeur null est retournée.
	 * @param stringValue Valeur de l'attribut. 
	 * @return La valeur castée
	 * Null si non entier
	 */
	public static Integer getIntValue(final String stringValue) {
		if (stringValue != null && !stringValue.isEmpty()) {
			try {
				return Integer.parseInt(stringValue.trim());
			} catch (NumberFormatException e) {
				return null;
			}
		} else {
			return null;
		}
	}
	
	/** Retourne la valeur pour un champ du formulaire qui doit être casté en entier.
	 * Si la valeur n'est pas à castée n'est pas un entier, la valeur null est retournée.
	 * @param stringValue Valeur de l'attribut. 
	 * @return La valeur castée (nombre flottant)
	 */
	public static Float getNumericValue(final String stringValue) {
		if (stringValue != null && !stringValue.isEmpty()) {
			try {
				return Float.parseFloat(stringValue.trim());
			} catch (NumberFormatException e) {
				return null;
			}
		} else {
			return null;
		}
	}
	
	
	/** Retourne la valeur pour un champ du formulaire qui doit être casté en boolean.
	 * 
	 * @param stringValue Valeur de l'attribut
	 * @return La valeur castée
	 */
	public static Boolean getBooleanValue(final String stringValue) {
		if (stringValue != null) {
			switch(stringValue.trim().toLowerCase()) {
				case "on": 
				case "true": 
				case "yes":
				case "1":
					return true;
				case "off": 
				case "no":
				case "false":
				case "0":
					return false;
				default: 
					return false;
			}
		}
		return null;
	}
	
	/** Retourne la valeur pour un champ du formulaire qui doit être une chaîne de caractère. 
	 * Si la valeur est vide, on retourne NULL.
	 * 
	 * @param stringValue Valeur de l'attribut
	 * @return La valeur 
	 * Null si vide
	 */
	public static String getStringValue(final String stringValue) {
		if (stringValue == null || "".equals(stringValue)) {
			return null;
		}
		return stringValue;
	}


	/** Est ce que le formulaire contient l'élément indiqué. 
	 * 
	 * @param element : le nom du paramètre
	 * @return OUI/NON
	 */
	public boolean contains(final String element) {
		return this.form.getNames().contains(element);
	}
	
	/** Renvoie la liste des paramètres du formulaire.
	 * 
	 * @return List de noms
	 */
	public Set<String> getNames() {
		return this.form.getNames();
	}
	/** Renvoie le nombre d'éléments du formulaire.
	 * 
	 * @return nombre
	 */
	public int size() {
		return this.form.getNames().size();
	}


	/** Récupère le formulaire associé à l'utilitaire. 
	 * @return the form
	 */
	public final Form getForm() {
		return form;
	}

	
	
}
