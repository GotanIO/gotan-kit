package io.gotan.kit.restlet;

import java.io.File;

/** Représentation d'un fichier uploadé avec ses MetaData
 * 
 * 
 * <br />
 * Project J-Tools <br /> <br />
 * All Rights Reserved - © Straton IT 
 * @author Damien Cuvillier <dcuvillier@straton-it.fr>
 * @since 16 avr. 2013
 *
 */
public class FileRepresentationBean {
	/** Fichier en question. */
	private File file;
	
	/** Nom d'origine du fichier. */
	private String name;
	
	/** Taille du fichier uploadé. */
	private long size; 
	
	/** Type de données déclaré. */
	private String type;

	/** Renvoie la variable file.
	 * @return le file
	 */
	public File getFile() {
		return file;
	}

	
	/* ACCESSORS */
	/** Modifie la valeur de file.
	 * @param file le file à définir
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/** Renvoie la variable name.
	 * @return le name
	 */
	public String getName() {
		return name;
	}

	/** Modifie la valeur de name.
	 * @param name le name à définir
	 */
	public void setName(String name) {
		this.name = name;
	}

	/** Renvoie la variable size.
	 * @return le size
	 */
	public long getSize() {
		return size;
	}

	/** Modifie la valeur de size.
	 * @param size le size à définir
	 */
	public void setSize(long size) {
		this.size = size;
	}

	/** Renvoie la variable type.
	 * @return le type
	 */
	public String getType() {
		return type;
	}

	/** Modifie la valeur de type.
	 * @param type le type à définir
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
	
}
