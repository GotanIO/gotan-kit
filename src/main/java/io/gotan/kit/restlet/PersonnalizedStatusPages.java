package io.gotan.kit.restlet;


import io.gotan.kit.GK;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Reference;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.service.StatusService;

public class PersonnalizedStatusPages extends StatusService {

    @Override
    public void setContactEmail(String contactEmail) {
        super.setContactEmail(GK.c("application.contact.email"));
    }

    @Override
    public void setHomeRef(Reference homeRef) {
        super.setHomeRef(homeRef);
    }

    @Override
    public void setOverwriting(boolean overwriting) {
        super.setOverwriting(true);
    }

    @Override
    public Representation getRepresentation(Status status, Request request, Response response) {
        StringBuilder rc = new StringBuilder();
        rc.append("<meta charset=\"utf-8\" />");
        rc.append("Error " + status.getCode());
        rc.append(": " + status.getDescription());
        return new StringRepresentation(rc.toString());
    }

    /* @see org.restlet.service.StatusService#getStatus(java.lang.Throwable, org.restlet.Request, org.restlet.Response)
     */
    @Override
    public Status getStatus(Throwable throwable, Request request,
                            Response response) {
        response.redirectTemporary("/error.html");
        return response.getStatus();
    }

}
