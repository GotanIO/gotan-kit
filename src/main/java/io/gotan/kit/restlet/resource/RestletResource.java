package io.gotan.kit.restlet.resource;

import com.google.gson.JsonObject;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentCollectionConverter;
import com.thoughtworks.xstream.hibernate.converter.HibernatePersistentSortedSetConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.AbstractJsonWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;
import io.gotan.kit.GK;
import io.gotan.kit.file.FileUtils;
import io.gotan.kit.restlet.FileRepresentationBean;
import io.gotan.kit.restlet.FormUtils;
import io.gotan.kit.restlet.ISO8601Converter;
import io.gotan.kit.restlet.UrlPath;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.restlet.data.*;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Options;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import java.io.*;
import java.util.*;

/**
 * Comportements génériques de Restlet. Bibliothèque de fonctions.
 * <p>
 * <br />
 * Project J-Tools <br />
 * <br />
 * All Rights Reserved - © Straton IT
 *
 * @author Damien Cuvillier <dcuvillier@straton-it.fr>
 * @since 16 avr. 2013
 */
public class RestletResource extends ServerResource {

    /**
     * Private Log4J Logger.
     */
    private static final Logger logger = LogManager.getLogger(RestletResource.class);

    /**
     * Tableau de paramètres.
     */
    private Map<String, FileItem> parameters;

    /**
     * Retourne le contexte d'application.
     * <p>
     * Attention, cela ne renvoie pas l'URL de base si on se situe derrière un
     * reverse-proxy. Voir la tâche <a href="http://redmine.straton-it.net/issues/1049">...</a>
     *
     * @return URL de base de l'application
     */
    public final String getApplicationContext() {
        return this.getRequest().getRootRef().toString();
    }

    /**
     * This function extract a String from URI.
     *
     * @param attributeName attribute name of the attribute to consider in URI
     * @return String attribute extracted from URI
     */
    protected final String getParameter(final String attributeName) {
        String stringValue = null;
        try {
            stringValue = this.getRequest().getAttributes().get(attributeName).toString();
        } catch (NullPointerException e) {
            // Si on trouve pas l'attribut dans l'url, on le cherche dans les
            // variables postées.
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Attribute %f not found in %f", attributeName, this.getRequest().toString()));
            }
        }
        return stringValue;
    }

    /**
     * This function extracts a string from the headers.
     *
     * @param headerName       header name of the header to consider in URI
     * @param isHeaderRequired if the header is required, the request will be
     *                         stopped (an Exception si raised by this.stop()
     *                         method)
     * @return String header extracted from URI
     * @throws ResourceException if this.stop() is executed
     */
    protected final String getHttpHeaderValue(final String headerName, boolean isHeaderRequired) {

        String headerValue = null;

        try {

            headerValue = this.getRequest().getHeaders().getFirstValue(headerName);

        } catch (NullPointerException e) {
            // ?
        }

        if (isHeaderRequired && headerValue == null) {
            this.stop(Status.CLIENT_ERROR_UNAUTHORIZED, GK.m("MissingHeader", headerName));
        }

        return headerValue;
    }

    /**
     * Récupère la liste des types acceptés dans la requêtes.
     * <ol>
     * <li>Récupère l'extension de l'URL qui prévaut sur le reste</li>
     * <li>Récupère la liste des mimeType de la directive Accept dans l'entête
     * HTTP</li>
     * </ol>
     *
     * @param explicitExtension L'extension attendue déclarée explicitement
     * @return La liste des types dans l'ordre à traiter.
     */
    protected final List<MediaType> getRequestedTypes(final String explicitExtension) {
        List<MediaType> types = new ArrayList<>();
        String requestExtension = null;
        if (explicitExtension != null) {
            requestExtension = explicitExtension;
        } else {
            requestExtension = this.getParameter("extension");
        }
        if (requestExtension != null) {
            types.add(new MediaType("text/" + requestExtension));
        }
        // Transforme un List<Preference<MediaType>> en List<MediaType>
        for (Preference<MediaType> pType : this.getRequest().getClientInfo().getAcceptedMediaTypes()) {
            types.add(pType.getMetadata());
        }
        return types;
    }

    /**
     * Récupère la liste des types acceptés dans la requêtes.
     * <ol>
     * <li>Récupère l'extension de l'URL qui prévaut sur le reste</li>
     * <li>Récupère la liste des mimeType de la directive Accept dans l'entête
     * HTTP</li>
     * </ol>
     *
     * @return La liste des types dans l'ordre à traiter.
     */
    protected final List<MediaType> getRequestedTypes() {
        return getRequestedTypes(null);
    }

    /**
     * Transforme un document en une StringReprésentation. Le transformer (JSON,
     * XML..) est choisi en fonction de la requête;
     *
     * @param document à transformer.
     * @return une StringRepresentation utilisable pour la réponse.
     */
    public StringRepresentation transformDocument(final Object document) {
        String outputString = null;
        if (document == null) {
            outputString = "{}";
        } else if ((document instanceof Collection && ((Collection<?>) document).isEmpty())) {
            outputString = "[]";
        }

        if (outputString == null) {
            outputString = this.getStreamConfig().toXML(document);
        }

        StringRepresentation sr = new StringRepresentation(outputString);
        sr.setMediaType(new MediaType("application/json"));
        sr.setDisposition(this.getDefaultDisposition());

        return sr;
    }

    /**
     * Définit les valeurs par défaut pour les entêtes HTTP. Permet notamment
     * d'afficher le contenu JSON en inline et non pas en attachement.
     *
     * @return Configuration
     */
    protected Disposition getDefaultDisposition() {
        Disposition disp = new Disposition();
        disp.setType(Disposition.TYPE_INLINE);
        return disp;
    }

    protected XStream getStreamConfig() {
        XStream xstream = null;
        xstream = new XStream(new JsonHierarchicalStreamDriver() {
            @Override
            public HierarchicalStreamWriter createWriter(final Writer writer) {
                return new JsonWriter(writer, AbstractJsonWriter.DROP_ROOT_MODE + AbstractJsonWriter.STRICT_MODE, new JsonWriter.Format("  ".toCharArray(), "\n".toCharArray(), JsonWriter.Format.COMPACT_EMPTY_ELEMENT));
            }
        });
        xstream.aliasSystemAttribute(null, "class");
        xstream.setMode(XStream.ID_REFERENCES);
        xstream.autodetectAnnotations(true);
        xstream.registerConverter(new ISO8601Converter());
        xstream.registerConverter(new HibernatePersistentCollectionConverter(xstream.getMapper()));
        xstream.registerConverter(new HibernatePersistentSortedSetConverter(xstream.getMapper()));
        return xstream;
    }

    /**
     * Stop current request.
     *
     * @param status  Statut de retour
     * @param message Message de l'erreur
     */
    protected void stop(final Status status, final String message) {
        logger.info(status + " > " + this.getRequest().getMethod().getUri() + " : " + message);
        throw new ResourceException(status.getCode(), message, "");
    }

    /**
     * Stop current request.
     *
     * @param status  Statut de retour
     * @param message Message de l'erreur
     */
    protected void stop(final int status, final String message) {
        logger.info(status + " > " + this.getRequest().getMethod().getUri() + " : " + message);
        throw new ResourceException(status, message);
    }

    protected final String getUrl() {
        UrlPath[] urlPath = this.getClass().getAnnotationsByType(UrlPath.class);
        return urlPath[0].value();
    }

    /**
     * Stop current request.
     *
     * @param status  Statut de retour
     * @param e Exception associée à l'erreur
     */
    protected void stop(final Status status, final Exception e) {
        throw new ResourceException(status, e);
    }

    /**
     * Vérifie sir les paramètres nommés sont chargés et non null
     *
     * @param form Formulaire d'entrée
     * @param params liste de paramètres
     */
    public void checkNotNullElements(Form form, String... params) {

        boolean check = !form.isEmpty();

        String missP = null;
        for (int i = 0; i < params.length; i++) {
            if (form.getFirst(params[i]) == null) {
                missP = params[i];
                logger.info(GK.m("MissingParameter", missP));
                check = false;

            }
        }
        if (!check) {
            this.stop(Status.CLIENT_ERROR_NOT_ACCEPTABLE, GK.m("MissingParameter", missP));
        }
    }

    /**
     * Vérifie si les paramètres nommés sont chargés et non nuls.
     *
     * @param jsonData Données d'entée
     * @param params liste de paramètres
     */
    public void checkNotNullElements(JsonObject jsonData, String... params) {

        boolean check = !jsonData.isJsonNull();

        String missP = null;
        for (String param : params) {
            if (!jsonData.getAsJsonObject().has(param)) {
                missP = param;
                logger.info(GK.m("MissingParameter", missP));
                check = false;

            }
        }
        if (!check) {
            this.stop(Status.CLIENT_ERROR_NOT_ACCEPTABLE, GK.m("MissingParameter", missP));
        }
    }

    /**
     * Récupère une instance du formulaire pour la requête courante.
     *
     * @return Form
     */
    protected Form getForm() {
        return this.getRequest().getResourceRef().getQueryAsForm();
    }

    /**
     * Récupère une instance de la classe utilitaire FormUtils pour la requête
     * courante.
     *
     * @return FormUtils
     */
    protected FormUtils getFormUtils() {
        return new FormUtils(this.getForm());
    }

    /**
     * Récupère une instance de la classe utilitaire FormUtils pour la requête
     * courante.
     *
     * @param form Formulaire posté
     * @return FormUtils
     */
    protected FormUtils getFormUtils(final Form form) {
        return new FormUtils(form);
    }

    /**
     * Récupère l'url relative dans l'API sans extension.
     *
     * @return String
     */
    public String getRelativeUrl() {
        return this.getRelativeUrl(false);

    }

    /**
     * Récupère l'url relative dans l'API sans extension.
     *
     * @param withExtension Avec ou sans extension
     * @return String
     */
    public String getRelativeUrl(final boolean withExtension) {
        String path = this.getRequest().getResourceRef().getPath();
        path = path.substring(path.indexOf("api/") + 4);
        if (path.lastIndexOf(".") != -1 && !withExtension) {
            path = path.substring(0, path.lastIndexOf("."));
        }
        return path;
    }

    /**
     * Récupère l'eventuelle extension de l'appel dans l'url.
     *
     * @return l'extension. "" si non existante.
     */
    public String getUrlExtension() {
        return FileUtils.getFileExtension(this.getRelativeUrl(true));
    }

    /* GESTION DE L'UPLOAD */

    /**
     * Encode par défaut des flux de récupérations de fichiers uploadés
     */
    private static final String DEFAULT_ENCODING = "UTF-8";

    /**
     * Récupère une réprésentation complète du fichier uploadé dans la requête
     * multipart. La méthode principale de gestion de la ressource doit contenir
     * l'annotation <code>   @Post("multipart/form-data")</code>
     * <p>
     * Ne fonctionne qu'avec un seul fichier.
     *
     * @param fieldName Nom du champ stockant le fichier attendu dnas le formulaire
     * @param entity    Representation du formulaire d'entrée
     * @return Représentation du fichier avec ses metadata
     * @throws Exception
     * @see <a href="http://restlet.org/learn/guide/2.0/extensions/fileupload">...</a>
     */
    protected FileRepresentationBean getUploadedFileRepresentation(Representation entity, final String fieldName) throws Exception {
        FileItem foundFileItem = this.getFile(entity, fieldName);
        if (foundFileItem != null) {
            return transformFileItemToRepresentation(foundFileItem);
        } else {
            this.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
            return null;
        }

    }

    /**
     * Transforme un FileItem en RepresentationBean.
     *
     * @param foundFileItem FleITem
     * @return Représentation d'un fichier avec Metadata
     * @throws Exception
     */
    public static FileRepresentationBean transformFileItemToRepresentation(final FileItem foundFileItem) throws Exception {
        if (foundFileItem == null) {
            return null;
        }
        FileRepresentationBean bean = new FileRepresentationBean();

        /*
         * On préserve le nom et l'extension du fichier en suffixe pour permettre une
         * reprise eventuelle au cas où le mime type n'est pas disponible par exemple
         */
        File returnedFile = File.createTempFile("restlet", "---" + foundFileItem.getName());

        foundFileItem.write(returnedFile);
        bean.setFile(returnedFile);

        bean.setName(foundFileItem.getName());
        bean.setType(foundFileItem.getContentType());
        bean.setSize(foundFileItem.getSize());

        return bean;
    }

    /**
     * Récupère le fichier uploadé dans la requête multipart. La méthode principale
     * de gestion de la ressource doit contenir l'annotation
     * <code>   @Post("multipart/form-data")</code>
     * <p>
     * Ne fonctionne qu'avec un seul fichier.
     *
     * @param fieldName Nom du champ stockant le fichier attendu dnas le formulaire
     * @param entity    Representation du formulaire d'entrée
     * @return Fichier temporaire
     * @throws Exception
     * @see <a href="http://restlet.org/learn/guide/2.0/extensions/fileupload">...</a>
     */
    protected final File getUploadedFile(final Representation entity, final String fieldName) throws Exception {
        FileItem foundFileItem = this.getFile(entity, fieldName);
        if (foundFileItem != null) {
            File returnedFile = File.createTempFile("restlet", "uploadResource");
            foundFileItem.write(returnedFile);
            return returnedFile;
        } else {
            // POST request with no entity.
            setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        }
        return null;
    }

    /**
     * Récupère le contenu du fichier uploadé dans la requête multipart. La méthode
     * principale de gestion de la ressource doit contenir l'annotation
     * <code>   @Post("multipart/form-data")</code>
     * <p>
     * Ne fonctionne qu'avec un seul fichier.
     *
     * @param fieldName Nom du champ stockant le fichier attendu dnas le formulaire
     * @param entity    Representation du formulaire d'entrée
     * @param encoding  Encodage de la chaine de sortie.
     * @return Contenu texte du fichier uploadé
     * @throws FileUploadException
     * @throws UnsupportedEncodingException
     * @throws Exception
     * @see <a href="http://restlet.org/learn/guide/2.0/extensions/fileupload">...</a>
     */
    protected String getUploadedFileContent(Representation entity, final String fieldName, final String encoding) throws FileUploadException, UnsupportedEncodingException {

        if (entity == null || fieldName == null) {
            return null;
        }
        FileItem file = this.getFile(entity, fieldName);
        if (file == null) {
            return null;
        }
        return file.getString(encoding);

    }

    /**
     * Récupère le contenu du fichier uploadé dans la requête multipart. <br />
     * <p>
     * Utilise l'encodage par défaut (constante DEFAULT_ENCODING)<br />
     * La méthode principale de gestion de la ressource doit contenir l'annotation
     * <code>   @Post("multipart/form-data")</code>
     * <p>
     * Ne fonctionne qu'avec un seul fichier.
     *
     * @param fieldName Nom du champ stockant le fichier attendu dnas le formulaire
     * @param entity    Representation du formulaire d'entrée
     * @return Contenu texte du fichier uploadé
     * @throws FileUploadException
     * @throws UnsupportedEncodingException
     * @throws Exception
     * @see <a href="http://restlet.org/learn/guide/2.0/extensions/fileupload">...</a>
     */
    protected String getUploadedFileContent(Representation entity, final String fieldName) throws FileUploadException, UnsupportedEncodingException {
        return this.getUploadedFileContent(entity, fieldName, DEFAULT_ENCODING);
    }

    /**
     * Récupère le premier fichier lié d'une requête multipart. La méthode
     * principale de gestion de la ressource doit contenir l'annotation
     * <code>   @Post("multipart/form-data")</code>
     * <p>
     * Ne fonctionne qu'avec un seul fichier.
     *
     * @param fieldName Nom du champ stockant le fichier attendu dnas le formulaire
     * @param entity    Representation du formulaire d'entrée
     * @return Contenu texte du fichier uploadé
     * @throws FileUploadException
     * @throws UnsupportedEncodingException
     * @throws Exception
     * @see <a href="http://restlet.org/learn/guide/2.0/extensions/fileupload">...</a>
     */
    private FileItem getFile(Representation entity, final String fieldName) throws FileUploadException {

        if (entity != null) {
            if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true)) {

                // The Apache FileUpload project parses HTTP requests which
                // conform to RFC 1867, "Form-based File Upload in HTML". That
                // is, if an HTTP request is submitted using the POST method,
                // and with a content type of "multipart/form-data", then
                // FileUpload can parse that request, and get all uploaded files
                // as FileItem.

                // 1/ Create a factory for disk-based file items
                DiskFileItemFactory factory = new DiskFileItemFactory();
                factory.setSizeThreshold(1000240);

                // 2/ Create a new file upload handler based on the Restlet
                // FileUpload extension that will parse Restlet requests and
                // generates FileItems.
                RestletFileUpload upload = new RestletFileUpload(factory);
                List<FileItem> items;

                // 3/ Request is parsed by the handler which generates a
                // list of FileItems
                items = upload.parseRequest(getRequest());

                // Process only the uploaded item called "fileToUpload" and
                // save it on disk

                for (final Iterator<FileItem> it = items.iterator(); it.hasNext(); ) {

                    FileItem fi = it.next();
                    if (fi.getFieldName().equals(fieldName)) {
                        return fi;
                    }
                }
                return null;

            }
        } else {
            // POST request with no entity.
            setStatus(Status.CLIENT_ERROR_BAD_REQUEST);

        }
        return null;

    }

    /**
     * Récupère tous les paramètres d'une requête multipart La méthode principale de
     * gestion de la ressource doit contenir l'annotation
     * <code>   @Post("multipart/form-data")</code>
     *
     * @param entity   Representation du formulaire d'entrée
     * @return L'ensemble des paramètres
     * @throws FileUploadException
     * @throws UnsupportedEncodingException
     * @throws Exception
     * @see <a href="http://restlet.org/learn/guide/2.0/extensions/fileupload">...</a>
     */
    public Map<String, FileItem> getAllRequestParameters(final Representation entity) throws FileUploadException {

        if (entity != null) {
            if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true)) {

                if (parameters != null && !parameters.isEmpty()) {
                    return parameters;
                }

                // The Apache FileUpload project parses HTTP requests which
                // conform to RFC 1867, "Form-based File Upload in HTML". That
                // is, if an HTTP request is submitted using the POST method,
                // and with a content type of "multipart/form-data", then
                // FileUpload can parse that request, and get all uploaded files
                // as FileItem.

                // 1/ Create a factory for disk-based file items
                DiskFileItemFactory factory = new DiskFileItemFactory();
                factory.setSizeThreshold(1000240);

                // 2/ Create a new file upload handler based on the Restlet
                // FileUpload extension that will parse Restlet requests and
                // generates FileItems.
                RestletFileUpload upload = new RestletFileUpload(factory);
                List<FileItem> items;

                // 3/ Request is parsed by the handler which generates a
                // list of FileItems
                items = upload.parseRequest(getRequest());

                // Process only the uploaded item called "fileToUpload" and
                // save it on disk

                HashMap<String, FileItem> requestParameters = new HashMap<>(items.size());

                for (final Iterator<FileItem> it = items.iterator(); it.hasNext(); ) {

                    FileItem fi = it.next();
                    requestParameters.put(fi.getFieldName(), fi);
                }
                return requestParameters;

            }
        } else {
            // POST request with no entity.
            setStatus(Status.CLIENT_ERROR_BAD_REQUEST);

        }
        return null;

    }

    protected File getFileFromDBBlob(byte[] blobContent) {
        File file = null;
        OutputStream os = null;
        if (blobContent == null) {
            return null;
        }

        String content = new String(blobContent);
        if (content.startsWith("data:")) {
            content = content.substring(content.indexOf(","));
        }
        blobContent = Base64.getDecoder().decode(content);
        try (InputStream is = new ByteArrayInputStream(blobContent)) {
            file = File.createTempFile(GK.c("application.name"), null);
            os = new FileOutputStream(file);
            int read = 0;
            byte[] bytes = new byte[8 * 1024];

            while ((read = is.read(bytes)) != -1) {
                os.write(bytes, 0, read);
            }
        } catch (IOException e) {
            logger.error("Unable to manage IO on BLOB transformation");

        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    logger.error("Unable to close outputstream");
                }

            }

        }
        return file;
    }

    /**
     * @param pParameters the parameters to set
     */
    public final void setParameters(final Map<String, FileItem> pParameters) {
        this.parameters = pParameters;
    }

    @Options
    public void getOptions() {
        logger.debug("Options");
    }
}
