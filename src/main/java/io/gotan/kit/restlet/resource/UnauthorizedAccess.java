package io.gotan.kit.restlet.resource;

import io.gotan.kit.GK;

public class UnauthorizedAccess extends RuntimeException {
	/**
	 * Generated Serial ID.
	 */
	private static final long serialVersionUID = -6570295452977512115L;

	public UnauthorizedAccess(String path, String user) {
		super (GK.m("Unauthorized"));
	}
}
