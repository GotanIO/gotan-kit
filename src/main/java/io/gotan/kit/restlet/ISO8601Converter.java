package io.gotan.kit.restlet;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import io.gotan.kit.GK;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Convertir une date en millisecond et vice-versa.
 * 
 * 
 * 
 */
public class ISO8601Converter implements SingleValueConverter {

  /** Log4J Logger for current class. */
  private static Logger logger = LogManager.getLogger(ISO8601Converter.class);

  /*
   * (non-Javadoc)
   * 
   * @see com.thoughtworks.xstream.converters.ConverterMatcher#canConvert(java.
   * lang.Class)
   */
  @SuppressWarnings("rawtypes") // Can't parametize because of overriding on xstrem
  @Override
  public final boolean canConvert(final Class pType) {
    return pType.equals(Date.class) || pType.getSuperclass().equals(Date.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.thoughtworks.xstream.converters.SingleValueConverter#toString(java
   * .lang.Object)
   */
  @Override
  public final String toString(final Object pObj) {
    if (pObj == null) {
      return null;
    }
    return ISO8601Converter.convertDate((Date) pObj);
  }

  /**
   * Convertit une date en chaîne de caractère
   * 
   * @param date Date à convertir
   * @return Chaine au format ISO 8601
   */
  public static final String convertDate(Date date) {
    if (date == null) {
      return "";
    } else {
      SimpleDateFormat formatter = new SimpleDateFormat(GK.c("date.format"));
      formatter.setTimeZone(TimeZone.getDefault());
      return formatter.format(date);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.thoughtworks.xstream.converters.SingleValueConverter#fromString(java
   * .lang.String)
   */
  @Override
  public final Object fromString(final String pStr) {
    return ISO8601Converter.parse(pStr);
  }

  public static final Date parse(String pStr) {
    try {
      return parse(pStr, false);
    } catch (ParseException e) {
      logger.error(String.format("Unable to parse %s to date (Not ISO8601 format)", pStr));
      return new Date();
    }
  }

  public static final Date parse(String pStr, boolean throwException) throws ParseException {
    if (pStr == null || "".equals(pStr)) {
      return null;
    }
    /* Ces différents formats sont utiles lors de l'import */
    SimpleDateFormat formatter;
    boolean needNormalization = false;
    if (pStr.endsWith("Z")) {
      formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
      needNormalization = true;
    } else if (pStr.endsWith("UTC")) {
      formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    } else {
      formatter = new SimpleDateFormat(GK.c("date.format"));
      needNormalization = true;
    }
    formatter.setTimeZone(TimeZone.getDefault());
    formatter.setLenient(false);
    Date date = null;
    try {
      date = formatter.parse(pStr);
      /* permet de normaliser les dates en ajoutant une milliseconde */
      if (needNormalization) {
        date.setTime(date.getTime() + 1);
      }
    } catch (ParseException e) {
      if (throwException) {
        throw e;
      } else {
        date = new Date();
      }
    }
    return date;
  }

}