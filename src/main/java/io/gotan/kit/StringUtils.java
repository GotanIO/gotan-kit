package io.gotan.kit;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Classe utilitaire pour la manipulation de chaine de caractères.
 * 
 * 
 * 
 * @author damiencuvillier
 *
 */
public class StringUtils {

	/** Inclure les chiffres. */
	public final static int WITH_NUMERAL = 0b1;
	private final static String NUMERAL = "1234567890";
	/** Inclure les lettres majuscules. */
	public final static int WITH_LOWERCASE = 0b10;
	private final static String LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
	/** Inclure les lettres en minuscules. */
	public final static int WITH_UPPERCASE = 0b100;
	private final static String UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	/** Inclure des caractères spéciaux. */
	public final static int WITH_SPECIALCHARS = 0b1000;
	private final static String SPECIAL_CHARS = "$€@#&§-_+=";

	/**
	 * Generer une chaine de texte aléatoire.
	 * 
	 * @param length
	 *            Longueur de la chaîne
	 * @param includedChars
	 *            Paramètres de génération des caractères. Inclusion. Utiliser
	 *            les constantes de la classe
	 * @return La chaine ainsi générée
	 */
	public static String generateRandomString(final int length,
			int... includedChars) {

		// Selection des caractères autorisés
		int configuration = 0;
		for (int i = 0; i < includedChars.length; i++) {
			configuration |= includedChars[i];
		}

		StringBuilder charsList = new StringBuilder();
		if ((configuration & WITH_NUMERAL) == WITH_NUMERAL) {
			charsList.append(NUMERAL);
		}
		if ((configuration & WITH_LOWERCASE) == WITH_LOWERCASE) {
			charsList.append(LOWERCASE);
		}
		if ((configuration & WITH_UPPERCASE) == WITH_UPPERCASE) {
			charsList.append(UPPERCASE);
		}
		if ((configuration & WITH_SPECIALCHARS) == WITH_SPECIALCHARS) {
			charsList.append(SPECIAL_CHARS);
		}

		// Generation de l'aléa
		char[] chars = charsList.toString().toCharArray();
		StringBuilder sb = new StringBuilder();
		for (int x = 0; x < length; x++) {
			int i = (int) Math.floor(Math.random() * chars.length);
			sb.append(chars[i]);
		}
		return sb.toString();
	}

	/**
	 * Encode la chaîne en SHA1.
	 * 
	 * @param password Chaine à encrypter
	 * @return Chaine encryptée
	 * @throws NoSuchAlgorithmException  Exception si l'algo n'est pas disponible dans le JDK
	 * @deprecated Use SHA256 instead
	 */
	@Deprecated
	public static String sha1(String password) throws NoSuchAlgorithmException {
		return new String(byteArrayToHexString(MessageDigest.getInstance("SHA1").digest(password.getBytes())));
	}
	 /** Encode la chaîne en SHA256.
	 * 
	 * @param password Chaine à encrypter
	 * @return Chaine encryptée
	 * @throws NoSuchAlgorithmException  Exception si l'algo n'est pas disponible dans le JDK
	 */
	public static String sha256(String password) throws NoSuchAlgorithmException {
		return new String(byteArrayToHexString(MessageDigest.getInstance("SHA-256").digest(password.getBytes())));
	}
	
	public static String byteArrayToHexString(byte[] b) {
		  String result = "";
		  for (int i=0; i < b.length; i++) {
		    result +=
		          Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
		  }
		  return result;
		}
}
