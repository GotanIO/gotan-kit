package io.gotan.kit.exceptions;

import io.gotan.kit.GK;

/** Exception sur un événement métier et une donnée non trouvée. 
 * 
 * @author damiencuvillier
 *
 */
public class NotFoundException extends BusinessException {

	/** Generated Serialization ID */
	private static final long serialVersionUID = 204271577597634162L;
	/** Default Constructor. */
	public NotFoundException(String message) {
		super(GK.m("NotFoundData") + " / " + message);
	}

}
