package io.gotan.kit.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** Problème de consistance de données. 
 * 
 * @author Damien Cuvillier <damien@gotan.io>  
 * Built by [Gotan](http://gotan.io)  
 *@license Apache2 
 * @since 9 févr. 2015
 */
public class ConsistencyDataException extends RuntimeException {
	
	/** Generated Serialiazation ID.
	 */
	private static final long serialVersionUID = 5421650920938658855L;

	/** Logger Static. */
	private static final Logger LOGGER = LogManager.getLogger(ConsistencyDataException.class);
	
	/** Message de l'exception. 
	 * @param message Message de l'exception. 
	 * */
	private String message;
	
	/** Default Constructor. 
	 * @param pMessage : le message explicatif de l'erreur. 
	 **/
	public ConsistencyDataException(final String pMessage) {
		this.message  = pMessage;
		LOGGER.error(pMessage);
	}
	
	/** Message. 
	 * @return le message de l'exception. 
	 * 
	 **/
	public final String getMessage() {
		return message;
	}
	
}
