package io.gotan.kit.exceptions;

import io.gotan.kit.GK;

/** Exception sur un événement métier et une donnée non trouvée. 
 * 
 * @author Damien Cuvillier <damien@gotan.io>  
 * Built by [Gotan](http://gotan.io)  
 *@license Apache2 
 * @since 3 févr. 2015
 */
public class UnauthorizedException extends BusinessException {

	/** Generated Serialization ID
	 */
	private static final long serialVersionUID = 2226875652795882705L;

	/** Default Constructor. 
	 * @param message Message personnalisée d'erreur
	 **/
	public UnauthorizedException(final String message) {
		super(GK.m("Unauthorized") + " / " + message);
	}

}
