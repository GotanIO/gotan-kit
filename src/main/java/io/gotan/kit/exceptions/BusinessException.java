package io.gotan.kit.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** Exception Metier. 
 * 
 * En général liéé aux données*/
public class BusinessException extends RuntimeException {
	/** Generated Serialization ID. */
	private static final long serialVersionUID = -8861421052132538556L;

	/** Log4J Logger for current class. */
	private static Logger logger = LogManager.getLogger(BusinessException.class);
	
	/** Message d'erreur. */
	private String message;
	/** Default Contructor. 
	 * @param pMessage message d'erreur */
	public BusinessException(final String pMessage) { 
		logger.error(pMessage);
		this.message = pMessage;
	}
	@Override
	public final String toString() {
		return this.getClass().getName()
				+ ": " + this.message 
				+ " (" + this.getCause() + ")";
	}
	/** Default Getter.
	 * @return the message
	 */
	public final String getMessage() {
		return message;
	}
	/** Default Setter.
	 * @param pMessage the message to set
	 */
	public final void setMessage(final String pMessage) {
		this.message = pMessage;
	}
	
	
	
}
