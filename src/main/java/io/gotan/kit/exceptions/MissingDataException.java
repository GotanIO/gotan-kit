package io.gotan.kit.exceptions;

import io.gotan.kit.GK;


/** Exception lorsqu'il manque une donnée importante. 
 * 
 * @author damiencuvillier
 *
 */
public class MissingDataException extends BusinessException {

	/** Generated Serialization ID.  */
	private static final long serialVersionUID = -1756246719423231719L;
	
	public MissingDataException(String missingDescription) {
		super(GK.m("missing.data", missingDescription));
	}
}
