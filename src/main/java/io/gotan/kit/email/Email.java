package io.gotan.kit.email;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.google.common.collect.Lists;

public class Email {

	/**
	 * Expéditeur de l'email.
	 */
	private InternetAddress sender;

	/**
	 * Destinataire principaux de l'email.
	 */
	private List<InternetAddress> toRecipients;

	/**
	 * Destinataires en copie.
	 */
	private List<InternetAddress> ccRecipients;

	/**
	 * Copies cachées.
	 */
	private List<InternetAddress> bccRecipients;

	/**
	 * Sujet du mail.
	 */
	private String subject;


	/**
	 * Contenu HTML de l'email.
	 */
	private String htmlContent;

	/**
	 * Contenu en pure texte.
	 */
	private String textContent;
	/**
	 * Liste des pièces jointes
	 */
	private List<File> attachments;

	public int getRecipientCount() {
		return this.getToRecipients().size()
				+ this.getCcRecipients().size()
				+ this.getBccRecipients().size();
	}

	/**
	 * @return the sender
	 */
	public final InternetAddress getSender() {
		return sender;
	}

	/**
	 * @param sender the sender to set
	 */
	public final void setSender(InternetAddress sender) {
		this.sender = sender;
	}

	public final void setSender(String sender) throws AddressException, UnsupportedEncodingException {
		InternetAddress ia = new InternetAddress(sender);
		ia.setPersonal(ia.getPersonal(), "UTF-8");
		this.setSender(ia);
	}

	/**
	 * @return the toRecipients
	 */
	public final List<InternetAddress> getToRecipients() {
		if (this.toRecipients == null) {
			this.toRecipients = Lists.newArrayList();
		}
		return toRecipients;
	}

	/**
	 * @param toRecipients the toRecipients to set
	 */
	public final void setToRecipients(List<InternetAddress> toRecipients) {
		this.toRecipients = toRecipients;
	}

	/**
	 * @return the ccRecipients
	 */
	public final List<InternetAddress> getCcRecipients() {
		if (this.ccRecipients == null) {
			this.ccRecipients = Lists.newArrayList();
		}
		return ccRecipients;
	}

	/**
	 * @param ccRecipients the ccRecipients to set
	 */
	public final void setCcRecipients(List<InternetAddress> ccRecipients) {
		this.ccRecipients = ccRecipients;
	}

	/**
	 * @return the bccRecipients
	 */
	public final List<InternetAddress> getBccRecipients() {
		if (this.bccRecipients == null) {
			this.bccRecipients = Lists.newArrayList();
		}
		return bccRecipients;
	}

	/**
	 * @param bccRecipients the bccRecipients to set
	 */
	public final void setBccRecipients(List<InternetAddress> bccRecipients) {
		this.bccRecipients = bccRecipients;
	}

	/**
	 * @return the subject
	 */
	public final String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public final void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the htmlContent
	 */
	public final String getHtmlContent() {
		return htmlContent;
	}

	/**
	 * @param htmlContent the htmlContent to set
	 */
	public final void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}

	/**
	 * @return the textContent
	 */
	public final String getTextContent() {
		return textContent;
	}

	/**
	 * @param textContent the textContent to set
	 */
	public final void setTextContent(String textContent) {
		this.textContent = textContent;
	}

	public final void addToRecipient(InternetAddress address) {
		this.getToRecipients().add(address);
	}

	public final void addToRecipient(String emailAddress) throws AddressException, UnsupportedEncodingException {
		InternetAddress ia = new InternetAddress(emailAddress);
		ia.setPersonal(ia.getPersonal(), "UTF-8");
		this.addToRecipient(ia);
	}

	public void addAttachment(final File file) {
		if (this.attachments == null) {
			this.attachments = new ArrayList<File>();
		}
		this.attachments.add(file);
	}

	public List<File> getAttachments() {
		return this.attachments;
	}

	public void setAttachments(List<File> attachments) {
		this.attachments = attachments;
	}
}