package io.gotan.kit.email;

import com.google.common.collect.Iterables;
import io.gotan.kit.GK;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.Properties;

public class EmailUtils {

	/** Log4J Logger for current class. */
	private static Logger logger = LogManager.getLogger(EmailUtils.class);

	/** ENvoi un email simplement à u destinataire. */
	public static void sendEmail(Email email) {
		Properties properties = new Properties();
		properties.put("mail.mime.charset", "UTF-8");
		properties.setProperty("mail.transport.protocol", "smtp");
		if (GK.c("mail.smtp.ssl.protocols") == null) {
			properties.setProperty("mail.smtp.ssl.protocols", "TLSv1.2");
		} else {
			properties.setProperty("mail.smtp.ssl.protocols", GK.c("mail.smtp.ssl.protocols"));
		}


		properties.setProperty("mail.smtp.host", GK.c("mail.smtp.host"));
		if (GK.c("mail.smtp.port") != null) {
			try {

				properties.setProperty("mail.smtp.port", GK.c("mail.smtp.port"));

				// SSL management
				if (Integer.parseInt(GK.c("mail.smtp.port")) != 25) {
					logger.debug("Enable SSL Trust for specific host and force startTLS");
					properties.setProperty("mail.smtp.starttls.enable", "true");
					properties.setProperty("mail.smtp.starttls.required", "true");
					properties.setProperty("mail.smtp.ssl.trust", GK.c("mail.smtp.host"));
					properties.setProperty("mail.smtp.starttls.required", "true");
				}

			} catch (NumberFormatException nfe) {
				logger.fatal("SMTP port number is not a right number : " + GK.c("mail.smtp.port"));
				throw nfe;
			}
		}

		if (GK.c("mail.smtp.user") != null) {
			properties.setProperty("mail.smtp.auth", "true");
			properties.setProperty("mail.smtp.user", GK.c("mail.smtp.user"));
		}
		if (email.getSender() == null) {
			try {
				String fullSender = "\"" + GK.c("mail.smtp.defaultSenderName") + "\" ";
				fullSender += "<" + GK.c("mail.smtp.defaultSender") + ">";
				properties.setProperty("mail.from", fullSender);
			} catch (MissingResourceException mre) {
				properties.setProperty("mail.from", GK.c("mail.smtp.defaultSender"));
			}
		} else {
			properties.setProperty("mail.from", email.getSender().toString());
		}
		Session session = Session.getInstance(properties);

		// 2 -> Création du message
		logger.trace("Preparing email message");
		MimeMessage message = new MimeMessage(session);
		try {
			message.setSubject(email.getSubject());
			message.setSentDate(new Date());

			// Construction du contenu
			Multipart multipart = new MimeMultipart();

			/* Contenu texte */
			BodyPart messageBodyPart = new MimeBodyPart();
			if (email.getTextContent() != null) {
				messageBodyPart.setText(email.getTextContent());
			}
			if (email.getHtmlContent() != null) {
				messageBodyPart.setContent(email.getHtmlContent(), "text/html; charset=utf-8");
			}
			multipart.addBodyPart(messageBodyPart);

			// Gestion des pièces jointes
			if (email.getAttachments() != null && email.getAttachments().size() > 0) {
				for (File file : email.getAttachments()) {
					if (file == null){
						continue;
					}
					BodyPart fileBodyPart = new MimeBodyPart();
					DataSource source = new FileDataSource(file.getAbsolutePath());
					fileBodyPart.setDataHandler(new DataHandler(source));
					fileBodyPart.setHeader("Content-Transfer-Encoding", "base64");
					fileBodyPart.setFileName(file.getName());
					multipart.addBodyPart(fileBodyPart);
				}
			}
			message.setContent(multipart);

			// Gestion des destinataires
			message.setFrom(new InternetAddress(properties.getProperty("mail.from")));
			for (InternetAddress address : email.getToRecipients()) {
				message.addRecipient(RecipientType.TO, address);
			}
			for (InternetAddress address : email.getCcRecipients()) {
				message.addRecipient(RecipientType.CC, address);
			}
			for (InternetAddress address : email.getBccRecipients()) {
				message.addRecipient(RecipientType.BCC, address);
			}

		} catch (MessagingException e) {
			logger.error("Unable to build email template : " + e.getMessage());
			return;
		}

		Address[] addresses = new Address[email.getRecipientCount()];
		int c = 0;
		for (InternetAddress a : Iterables.concat(email.getToRecipients(), email.getCcRecipients(), email.getBccRecipients())) {
			addresses[c++] = a;
		}
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("Sending email to %d addresses with subject '%s' on smtp %s", addresses.length,
					email.getSubject(), properties.getProperty("mail.smtp.host")));
		}

		// Envoi
		Transport transport = null;
		try {
			if (GK.c("mail.smtp.user") != null) {
				transport = session.getTransport("smtp");
				transport.connect(GK.c("mail.smtp.user"), GK.c("mail.smtp.password"));
			} else {
				// Sendmail direct
				transport = session.getTransport();
			}
			transport.sendMessage(message, addresses);
		} catch (MessagingException e) {
			logger.error(String.format("Unable to send email with SMTP %s:%s (%s) due to '%s'",
					properties.getProperty("mail.smtp.host"), properties.getProperty("mail.smtp.port"),
					properties.getProperty("mail.smtp.user"), e.getMessage()));
			logger.debug(e);
		} finally {
			if (transport != null && transport.isConnected()) {
				try {
					transport.close();
				} catch (MessagingException e) {
					logger.error("Unable to close SMTP transport session");
				}
			}
		}

	}

}
