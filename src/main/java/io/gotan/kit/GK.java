package io.gotan.kit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/** Raccourcis de programmation pour éviter les codes trops verbeux.
 * <br />
 * Cette classe donne un accès rapide et simple aux fonctionnalités les plus 
 * utilisées de GotanKit
 * @author Damien Cuvillier
 *
 */
public class GK {
	
	/** Log4J Logger for current class. */
	private static final Logger logger = LogManager.getLogger(GK.class);
	
	/** Constructeur privé. 
	 * <br />
	 * L'ensemble des méthodes sont statiques. Il s'agit d'une classe utilitaire.
	 */
	protected GK() {
		
	}
	/** Récupère la traduction d'un message dans le fichier du bundle.
	 * <br />
	 * @param key Clé d'entrée dans le fichier de messages
	 * @param parameters la liste des paramètres de la chaîne.
	 * @return Le message traduit
	 */
	public static String m(final String key, final String... parameters) {
		return Message.getInstance().getMessage(key, parameters);
	}
	/** Récupère la valeur d'une variable de configuration.
	 * <br />
	 * Le fichier de configuration doit figurer dans le classpath
	 * et s'appeler Configuration.properties
	 * @param keyName Nom de la propriété.
	 * @return La valeur de la variable
	 */
	public static String c(final String keyName) {
		try {
			return ResourceBundle.getBundle("Configuration").getString(keyName);
		} catch (MissingResourceException mre) {
			logger.error("Not found configuration variable " + keyName);
			return null;
		}
	}
	/** Récupère la valeur d'une variable de configuration parsée en entier.
	 * <br />
	 * Le fichier de configuration doit figurer dans le classpath
	 * et s'appeler Configuration.properties
	 * @param keyName Nom de la propriété.
	 * @return La valeur de la variable parsée en entier
	 */
	public static int ci(final String keyName) {
		String param = GK.c(keyName);
		return Integer.parseInt(param);
	}
	
	/** Récupère la valeur d'une variable de configuration parsée en booléen.
	 * <br />
	 * Le fichier de configuration doit figurer dans le classpath
	 * et s'appeler Configuration.properties
	 * @param keyName Nom de la propriété.
	 * @return La valeur de la variable parsée en boolean
	 */
	public static boolean cb(final String keyName) {
		String param = GK.c(keyName);
		return Boolean.parseBoolean(param);
	}
	
	/** Est ce que l'application est en mode debug */
	public static boolean isDebug(){
		return cb("application.debug");
	}
}
