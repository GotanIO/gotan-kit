package io.gotan.kit;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;


/**
 * Gestion facilitée des messages internationalisés. Tous les messages seront
 * stockés dans le fichier Message_FR.properties qui doit figurer dans le
 * classpath.
 * 
 *
 */
public final class Message {

	/** Nom du fichier contenant les messages dans le CLASSPATH. */
	private static final String MESSAGES_FILENAME = "Messages";

	/** Niveau de log pour les messages indiquant des manques. */
	private static final Level LOGGER_LEVEL = Level.WARN;

	/** Liste des messages de l'application. */
	private ResourceBundle messageBundle;

	/** Private Log4J Logger. */
	private static Logger logger = LogManager.getLogger(Message.class);

	/** Langue de recherche de l'information. */
	private Locale locale;

	/** Instance de la classe. Permet de définir un singleton */
	private static Message instance;

	/** Fichier de ressource activé. */
	private String activeResourceFile;

	/* *************** Constructors & Instantiations ***************** */

	/**
	 * Default Constructor. Gets Locale from Default System value
	 * 
	 * @throws MissingResourceException
	 *             Le fichier de resources n'est pas trouvé
	 */
	private Message() throws MissingResourceException {
		this(null);
	}

	/**
	 * Constructor with Locale. If Locale is null, set locale to default value
	 * 
	 * @param pLocale
	 *            Set locale language
	 * @throws MissingResourceException
	 *             Le fichier de resources n'est pas trouvé
	 */
	private Message(final Locale pLocale) throws MissingResourceException {
		if (pLocale == null) {
			this.locale = Locale.getDefault();
		} else {
			this.locale = pLocale;
		}
		try {
			messageBundle = ResourceBundle.getBundle(MESSAGES_FILENAME,
					this.locale, new Control() {
						// Gestion des absences de fichier localisés
						public Locale getFallbackLocale(final String baseName,
								final Locale pLocale) {
							Locale fallbackLocale = super.getFallbackLocale(
									baseName, pLocale);
							logger.log(LOGGER_LEVEL,
									"Can't find " + pLocale.getLanguage() + "("
											+ pLocale.getCountry()
											+ ") language in resources. "
											+ "Back to default language file.");
							return fallbackLocale;
						}

						public String toBundleName(final String baseName,
								final Locale pLocale) {
							activeResourceFile = super.toBundleName(baseName,
									pLocale);
							return activeResourceFile;
						}

						public ResourceBundle newBundle(String baseName,
								Locale locale, String format,
								ClassLoader loader, boolean reload)
								throws IllegalAccessException,
								InstantiationException, IOException {
							// The below is a copy of the default
							// implementation.
							String bundleName = toBundleName(baseName, locale);
							String resourceName = toResourceName(bundleName,
									"properties");
							ResourceBundle bundle = null;
							InputStream stream = null;
							if (reload) {
								URL url = loader.getResource(resourceName);
								if (url != null) {
									URLConnection connection = url
											.openConnection();
									if (connection != null) {
										connection.setUseCaches(false);
										stream = connection.getInputStream();
									}
								}
							} else {
								stream = loader
										.getResourceAsStream(resourceName);
							}
							if (stream != null) {
								try {
									// Only this line is changed to make it to
									// read properties files as UTF-8.
									bundle = new PropertyResourceBundle(
											new InputStreamReader(stream,
													"UTF-8"));
								} finally {
									stream.close();
								}
							}
							return bundle;
						}

					});
		} catch (MissingResourceException mre) {
			logger.log(LOGGER_LEVEL, "Unable to load " + MESSAGES_FILENAME
					+ ".properties resource file in ClassPath");
			throw mre;
		}
	}

	/**
	 * Get the single instance of this class.
	 * 
	 * @param pLocale
	 *            Set locale language
	 * @return Unique instance of class Message
	 * @throws MissingResourceException
	 *             Le fichier de resources n'est pas trouvé
	 */
	public static Message getInstance(final Locale pLocale)
			throws MissingResourceException {
		// On charge le fichier de langue si la langue a changée
		// où s'il s'agit de la première instantiation.
		if (instance == null || pLocale != instance.getLocale()) {
			instance = new Message(pLocale);
		}
		return instance;
	}

	/**
	 * Get the single instance of this class.
	 * 
	 * @return Unique instance of class Message
	 */
	public static Message getInstance() {
		if (instance == null) {
			instance = new Message(null);
		}
		return instance;
	}

	/**
	 * Récupère le message indiqué.
	 * 
	 * <br />
	 * Ajoute au fichier de log les entrées manquantes dans le fichier de la
	 * langue demandée.
	 * 
	 * @param key
	 *            Clé d'entrée dans le fichier des messages
	 * @return La chaîne de caractère internationalisée. Si elle n'est pas
	 *         trouvée, on retourne la clé d'entrée.
	 */
	public String getMessage(final String key) {
		try {
			return new String(
					this.getMessageBundle().getString(key).getBytes(), "UTF-8");
		} catch (MissingResourceException ex) {
			logger.log(
					LOGGER_LEVEL,
					"Message '"
							+ key
							+ "' has not been found in any context messages resource file");
			return key;
		} catch (UnsupportedEncodingException e) {
			logger.fatal("UTF-8 is not supported on this system");
			return this.getMessageBundle().getString(key);
		}

	}

	/**
	 * Récupère le message indiqué en formatant la réponse avec les variables
	 * mises en paramètres
	 * 
	 * <br />
	 * Ajoute au fichier de log les entrées manquantes dans le fichier de la
	 * langue demandée.
	 * 
	 * @param key
	 *            Clé d'entrée dans le fichier des messages
	 * @param parameters
	 *            Liste des paramètres passés pour formatter la chaîne
	 * @return La chaine de caractère internationalisée. Si elle n'est pas
	 *         trouvée, on retourne la clé d'entrée.
	 */
	public String getMessage(final String key, final String... parameters) {
		try {
			String message = this.getMessageBundle().getString(key);
			/*
			 * Il semble y avoir un bug sur la classe MessageFormat. Si les
			 * quotes ne sot pas doublés, on perd le formatage des variables
			 */

			message = message.replace("'", "''");
			MessageFormat format = new MessageFormat(message);
			message = format.format(parameters);
			return message;
		} catch (MissingResourceException ex) {
			logger.log(
					LOGGER_LEVEL,
					"Message '"
							+ key
							+ "' has not been found in any context messages resource file");
			return key;
		}

	}

	/* ******************* Accessors *************** */
	/**
	 * Récupère la liste des messages.
	 * 
	 * @return liste des messages
	 */
	private ResourceBundle getMessageBundle() {
		return messageBundle;
	}

	/**
	 * Récupère la langue recherchée.
	 * 
	 * @return la langue
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Modifie la langue de recherche des messages.
	 * 
	 * @param pLocale
	 *            langue
	 */
	public void setLocale(final Locale pLocale) {
		this.locale = pLocale;
	}

}
