/** Project GotanKit
 * 
 * Fonctions utilitaires et framework maison de la société Gotan
 * 
 * @author Damien Cuvillier <damien@gotan.io>  
 * Built by [Gotan](http://gotan.io)  
 *@license Apache2 
 * @since 11 mars 2015
 */
package io.gotan.kit;