package io.gotan.kit.data;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.Arrays;
import java.util.List;

/**
 * Gestion générique de la persistance de données.
 *
 * @param <T> Type de JavaBean persisté
 * @author Damien Cuvillier <damien@gotan.io>
 * Built by [Gotan](<a href="http://gotan.io">...</a>)
 * @license Apache2
 * @since 11 déc. 2014
 */

@Transactional(propagation = Propagation.REQUIRED)
public abstract class AbstractDaoImpl<T> implements AbstractDao<T> {

    /**
     * Typage de la DAO.
     */
    private final Class<T> typeParameterClass;

    /**
     * Gestionnaire de session.
     */
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Private Log4J Logger.
     */
    private static final Logger logger = LogManager.getLogger(AbstractDaoImpl.class);

    /**
     * Récupération de la connection.
     */
    private Method connectionMethod;

    /**
     * Constructeur privé. Permet d'obliger à définir le type persisté.
     *
     * @param pTypeParameterClass Type de la classe (POJO) à traiter
     */
    protected AbstractDaoImpl(final Class<T> pTypeParameterClass) {
        this.typeParameterClass = pTypeParameterClass;
    }

    /*
     */
    @Override
    public T save(final T bean) {
        Object o = this.getSession().save(bean);

        // Essaye de remettre l'id retourné dans l'objet d'origine
        // avec la reflexion
        Method m = null;
        int test = 0;
        Class<?> intOrIntegerOrLongType = null;

        Class<?>[] allowedTypes = {Integer.class, Long.class, int.class};
        for (; test <= 2; test++) {
            intOrIntegerOrLongType = allowedTypes[test];
            try {
                m = bean.getClass().getMethod("setId", intOrIntegerOrLongType);
                break;
            } catch (NoSuchMethodException e) {
                m = null;
                if (logger.isTraceEnabled()) {
                    logger.trace(String.format(
                            "Unable to setId on bean %f with parameter : %f" + "(I will try with int or Integer or Long).",
                            bean.getClass(), intOrIntegerOrLongType != null ? intOrIntegerOrLongType.getName() : "null"));

                }
            }
        }

        try {
            if (m != null && m.getParameterCount() == 1) {
                Class<?> parameterType = m.getParameterTypes()[0];
                if (Arrays.asList(allowedTypes).contains(parameterType)) {
                    m.invoke(bean, o);
                }
            }
        } catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            logger.fatal("Error with the method setId on bean " + bean.getClass());
        }

        return bean;
    }

    @Override
    public void delete(final T bean) {
        this.getSession().delete(bean);
    }

    @Override
    public T load(final Long id) {
        Object t = this.getSession().get(this.getTypeParameterClass(), id);
        if (this.getTypeParameterClass().isInstance(t)) {
            return this.getTypeParameterClass().cast(t);
        }
        return null;
    }

    @Override
    public T load(final Integer id) {
        Object t = this.getSession().get(this.getTypeParameterClass(), id);
        if (this.getTypeParameterClass().isInstance(t)) {
            return this.getTypeParameterClass().cast(t);
        }
        return null;
    }

    @Override
    public T load(final String id) {
        Object t = this.getSession().get(this.getTypeParameterClass(), id);
        if (this.getTypeParameterClass().isInstance(t)) {
            return this.getTypeParameterClass().cast(t);
        }
        return null;
    }

    @SuppressWarnings("unchecked") // Check with try catch statement
    @Override
    public T merge(final T o) {
        Object obj = this.getSession().merge(o);
        try {
            return (T) obj;
        } catch (ClassCastException cce) {
            logger.error(
                    "Unable to cast " + obj.getClass().getName() + " instance to " + this.typeParameterClass.getName() + " type");
            return null;
        }
    }

    /*
     */
    @Override
    @Transactional(readOnly = true)
    public List<T> getAll() {
        Session session = this.getSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        final CriteriaQuery<T> crit = builder.createQuery(typeParameterClass);
        Root<T> tRoot = crit.from(typeParameterClass);
        crit.select(tRoot);
        return session.createQuery(crit).getResultList();
    }

    /**
     * Récupère la session Hibernate.
     *
     * @return Session courante
     */
    public Session getSession() {
        Session session = sessionFactory.getCurrentSession();
        if (session == null) {
            logger.fatal("No session initialiazed");
        }
        return session;
    }

    /**
     * Récupère la connexion associée à la session hibernate.
     *
     * @return Connection
     * @see <a href="http://stackoverflow.com/questions/3526556/session-connection-deprecated-on-hibernate">...</a>
     */
    public Connection getConnection() {
        try {
            if (connectionMethod == null) {
                // reflective lookup to bridge between Hibernate 3.x and 4.x
                connectionMethod = this.getSession().getClass().getMethod("connection");
            }
            return (Connection) org.springframework.util.ReflectionUtils.invokeMethod(connectionMethod, this.getSession());
        } catch (NoSuchMethodException ex) {
            throw new IllegalStateException("Cannot find connection() method on Hibernate session", ex);
        }

    }

    /**
     * Default Getter.
     *
     * @return the typeParameterClass
     */
    public final Class<T> getTypeParameterClass() {
        return typeParameterClass;
    }

    /**
     * Default Getter.
     *
     * @return the sessionFactory
     */
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Default Setter.
     *
     * @param sessionFactory the sessionFactory to set
     */
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
