package io.gotan.kit.data;

import java.util.List;

/**
 * Gestion de l'accès aux données génériques avec les opérations de base.
 * 
 * @param <T>
 *            Type de JavaBean persisté
 * @author Damien Cuvillier <damien@gotan.io> Built by [Gotan](http://gotan.io)
 *        @license Apache2
 * @since 8 déc. 2014
 */
public interface AbstractDao<T> {

	/**
	 * Insertion.
	 * 
	 * @param bean
	 *            Le JavaBean à sauvegarder
	 * @return L'id inséré
	 */
	T save(T bean);

	/**
	 * Suppression.
	 * 
	 * @param bean
	 *            Javabean à supprimer
	 */
	void delete(T bean);

	/**
	 * Récupère une donnée à partir de son ID.
	 * 
	 * @param id
	 *            ID de l'élément
	 * @return Une donnée. Un POJO
	 */
	T load(Long id);
	
	/**
	 * Récupère une donnée à partir de son ID.
	 * 
	 * @param id
	 *            ID de l'élément
	 * @return Une donnée. Un POJO
	 */
	T load(Integer id);
        
        /**
	 * Récupère une donnée à partir de son ID.
	 * 
	 * @param id
	 *            ID de l'élément
	 * @return Une donnée. Un POJO
	 */
        T load(String id);

	/**
	 * Update du POJO en base.
	 * 
	 * public <T> T merge(final T o) { return (T)
	 * sessionFactory.getCurrentSession().merge(o); }
	 * 
	 * /** Sauvegarde aveugle : insert or update.
	 * 
	 * @param o
	 *            Javabean à persisté
	 * @return L'objet ainsi persisté. <br />
	 * 			Null si non castable avec le type T. (Avec trace de log) 
	 */
	T merge(T o);

	/**
	 * Charge tous les elements.
	 * 
	 * @return La liste de tous les éléments
	 */
	List<T> getAll();

}