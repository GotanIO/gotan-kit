/** Project GotanKit
 * 
 * Gestion de persistence de données
 * 
 * @author Damien Cuvillier <damien@gotan.io>  
 * Built by [Gotan](http://gotan.io)  
 *@license Apache2 
 * @since 12 mars 2015
 */
package io.gotan.kit.data;