package io.gotan.kit.date;

import static org.junit.Assert.assertTrue;

import java.util.GregorianCalendar;
import java.util.Locale;

import org.junit.Test;

public class DateUtilTest {

	@Test
	public void testWorkedDay() {
		assertTrue(!DateUtil.isWorkedDay(
				new GregorianCalendar(2014, 11, 25).getTime(), Locale.FRENCH));
		assertTrue(!DateUtil.isWorkedDay(
				new GregorianCalendar(2014, 10, 11).getTime(), Locale.FRENCH));
		assertTrue(!DateUtil.isWorkedDay(
				new GregorianCalendar(2015, 4, 14).getTime(), Locale.FRENCH));
		assertTrue(DateUtil.isWorkedDay(
				new GregorianCalendar(2015, 0, 2).getTime(), Locale.FRENCH));
		assertTrue(!DateUtil.isWorkedDay(
				new GregorianCalendar(2016, 4, 5).getTime(), Locale.FRENCH));
		assertTrue(!DateUtil.isWorkedDay(
				new GregorianCalendar(2016, 4, 16).getTime(), Locale.FRENCH));
		
		assertTrue(DateUtil.isWorkedDay(
				new GregorianCalendar(2015, 11, 24).getTime(), Locale.FRENCH));
	}

}
