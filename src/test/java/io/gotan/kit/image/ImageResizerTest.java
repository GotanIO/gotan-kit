
package io.gotan.kit.image;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.junit.Test;

/**
 * 
 * Dans ce jeu de test: fonction resizeImage sans condition de déformation test
 * agrandissement proportionnel * 2 avec une tolérance de déformation de 1
 * (100%) test agrandissement proportionnel * 2 avec une tolérance de
 * déformation de 0.1 (10%) test agrandissement proportionnel * 2 avec une
 * tolérance de déformation de 0.01 (1%) test rétrécissement proportionnel / 2
 * avec une tolérance de déformation de 0.01 (1%) test rétrécissement
 * proportionnel / 2 avec une tolérance de déformation de 0.1 (10%) test
 * rétrécissement proportionnel / 2 avec une tolérance de déformation de 1
 * (100%) test déformation extrême non proportionnelle (W/100, H*100) avec une
 * tolérance de déformation de 1 (100%) test quand il n'y a pas de
 * redimensionnement test hauteur de redimensionnement négative test largeur de
 * redimensionnement négative test largeur et hauteur de redimensionnement
 * négative test largeur redimensionnement = 0 test hauteur redimensionnement =
 * 0 test hauteur et largeur redimensionnement = 0 test hauteur et largeur
 * redimensionnement null
 */

public class ImageResizerTest {

  /**
   * teste la fonction resizeImage()
   */
  @Test
  public void resizeImageTest() {

    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension dimension = new Dimension(800, 600);

      BufferedImage newImage = ImageResizer.resizeImage(originalImage, dimension);

      int newHeight = newImage.getHeight(null);
      int newWidth = newImage.getWidth(null);

      assertEquals(new Dimension(newWidth, newHeight), dimension);
    } catch (IOException e) {
      fail("Unable to load base64 image");
    }
  }

  /**
   * teste la tolérance de la déformation de resizeImage() égale à 1 si
   * agrandissement *2
   */
  @Test
  public void resizeImageTolerance1Test() {

    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension((int) (originalImage.getWidth(null) * 2),
          (int) (originalImage.getHeight(null) * 2));
      double deformationTolerance = 1;

      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension, deformationTolerance);

      double ratio1 = originalImage.getWidth(null) / originalImage.getHeight(null);
      double ratio2 = newImage.getWidth(null) / newImage.getHeight(null);
      assertTrue("déformation acceptée", (ratio1 - ratio2) / ratio1 < deformationTolerance);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }
  }

  /**
   * teste la tolérance de la déformation de resizeImage() = 0.1
   */
  @Test
  public void resizeImageTolerance01Test() {

    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension((int) (originalImage.getWidth(null) * 2),
          (int) (originalImage.getHeight(null) * 2));
      double deformationTolerance = 0.1;

      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension, deformationTolerance);

      double ratio1 = originalImage.getWidth(null) / originalImage.getHeight(null);
      double ratio2 = newImage.getWidth(null) / newImage.getHeight(null);
      assertTrue("trop déformé", (ratio1 - ratio2) / ratio1 < deformationTolerance);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }
  }

  /**
   * teste la tolérance de la déformation de resizeImage() = 0.01 en cas
   * d'agrandissement par 2
   */
  @Test
  public void tolerance001Agrandissement() {

    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension(originalImage.getWidth(null) * 2, originalImage.getHeight(null) * 2);
      double deformationTolerance = 0.01;

      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension, deformationTolerance);

      double ratio1 = originalImage.getWidth(null) / originalImage.getHeight(null);
      double ratio2 = newImage.getWidth(null) / newImage.getHeight(null);
      assertTrue("trop déformé", (ratio1 - ratio2) / ratio1 < deformationTolerance);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }
  }

  /**
   * teste la tolérance de la déformation de resizeImage() = 0.01 en cas de
   * rétrécissement par 2
   */
  @Test
  public void tolerance001Retrecissement() {

    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension((int) (originalImage.getWidth(null) * 0.5),
          (int) (originalImage.getHeight(null) * 0.5));
      double deformationTolerance = 0.01;

      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension, deformationTolerance);

      double ratio1 = originalImage.getWidth(null) / originalImage.getHeight(null);
      double ratio2 = newImage.getWidth(null) / newImage.getHeight(null);
      assertTrue("trop déformé", (ratio1 - ratio2) / ratio1 < deformationTolerance);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }
  }

  /**
   * teste la tolérance de la déformation de resizeImage() = 0.1 en cas de
   * rétrécissement par 2
   */
  @Test
  public void tolerance01Retrecissement() {

    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension((int) (originalImage.getWidth(null) * 0.5),
          (int) (originalImage.getHeight(null) * 0.5));
      double deformationTolerance = 0.01;

      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension, deformationTolerance);

      double ratio1 = originalImage.getWidth(null) / originalImage.getHeight(null);
      double ratio2 = newImage.getWidth(null) / newImage.getHeight(null);
      assertTrue("trop déformé", (ratio1 - ratio2) / ratio1 < deformationTolerance);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }
  }

  /**
   * test tolérance de la déformation de resizeImage() = 1 en cas de
   * rétrécissement par 2
   */
  @Test
  public void tolerance1Retrecissement() {

    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension((int) (originalImage.getWidth(null) * 0.5),
          (int) (originalImage.getHeight(null) * 0.5));
      double deformationTolerance = 1;

      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension, deformationTolerance);

      double ratio1 = originalImage.getWidth(null) / originalImage.getHeight(null);
      double ratio2 = newImage.getWidth(null) / newImage.getHeight(null);
      assertTrue((ratio1 - ratio2) / ratio1 < deformationTolerance);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }
  }

  /**
   * test tolérance de la déformation de resizeImage() = 1 en cas de
   * redimensionnement sans conservation des proportions
   */
  @Test(expected = IndexOutOfBoundsException.class)
  public void disproportionExtreme() {

    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension((int) (originalImage.getWidth(null) / 100),
          (int) (originalImage.getHeight(null) * 100));
      double deformationTolerance = 1;

      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension, deformationTolerance);

      ArrayList<?> emptyList = new ArrayList<Object>();
      Object o = emptyList.get(0);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }
  }

  /**
   * test resizeImage() quand les images de départ et d'arrivée ont la même taille
   */
  @Test
  public void noredimension() {

    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension(originalImage.getWidth(null), originalImage.getHeight(null));
      double deformationTolerance = 0;

      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension, deformationTolerance);

      double ratio1 = originalImage.getWidth(null) / originalImage.getHeight(null);
      double ratio2 = newImage.getWidth(null) / newImage.getHeight(null);

      assertTrue("on attend la même taille", ratio1 == ratio2);
      // assertArraybytes

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }
  }

  /**
   * Test resizeImage() quand une nouvelle largeur est négative
   */
  @Test(expected = IllegalArgumentException.class)
  public void widthNegative() throws Exception {
    try {

      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension(-25, 200);
      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }

  }

  /**
   * Test resizeImage() quand une nouvelle hauteur est négative
   */
  @Test(expected = IllegalArgumentException.class)
  public void heightNegative() {
    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension(400, -200);
      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }

  }

  /**
   * Test resizeImage() quand les 2 nouvelles dimensions sont négatives
   */
  @Test(expected = IllegalArgumentException.class)
  public void negativeDimensions() {
    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension(-400, -200);
      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }

  }

  /**
   * Test resizeImage() quand la nouvelle largeur = 0
   */
  @Test(expected = IllegalArgumentException.class)
  public void width0() {
    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension(0, 200);
      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }

  }

  /**
   * Test resizeImage() quand la nouvelle hauteur = 0
   */
  @Test(expected = IllegalArgumentException.class)
  public void height0() {
    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension(400, 0);
      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }

  }

  /**
   * Test resizeImage() quand nouvelle hauteur = 0 et nouvelle largeur = 0
   */
  @Test(expected = IllegalArgumentException.class)
  public void width0height0() {
    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension(0, 0);
      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }

  }

  /**
   * Test resizeImage() quand nouvelle hauteur et nouvelle largeur sont null
   */
  @Test(expected = IllegalArgumentException.class)
  public void widthnullheightnull() {
    try {
      BufferedImage image = this.generateImage();
      BufferedImage originalImage = image;
      Dimension newDimension = new Dimension();
      BufferedImage newImage = ImageResizer.resizeImage(originalImage, newDimension);

    } catch (IOException e) {
      fail("Unable to load base64 image");
    }

  }

  private BufferedImage generateImage() throws IOException {
    BufferedImage imageInBytes = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("images/b64.png"));
    return imageInBytes;

  }
}
