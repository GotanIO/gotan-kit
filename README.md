## Gotan Kit

Toolkit for JEE developer. 

* Restlet
* File & Charset management
* Email Utils
* Date 
* Abstract Dao for generic access
* Translation shortcuts




### Maven

Dependency block
```
<dependency>
  <groupId>io.gotan.os</groupId>
  <artifactId>java-kit</artifactId>
  <version>1.2.0.5</version>
</dependency>
```   

And add following repository in `<repositories>` block of your pom.xml
```
<repository>
  <id>gotan-os</id>
  <name>Gotan OpenSource Contributions</name>
  <url>https://repository.dev.gotan.io/repository/gotan.os/</url>
</repository>
```


See [binary repository details](https://repository.dev.gotan.io/#browse/browse:gotan.os:gotan%2Fgotan.kit%2F1.0.0.20-SNAPSHOT%2F1.0.0.20-20200912.063901-1) for other dependency tools (SBT, IVY, ... ). 